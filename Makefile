.PHONY: build

REGISTRY_NAMESPACE = qiushaocloud
VERSION = latest

build:
	docker build -f Dockerfile -t $(REGISTRY_NAMESPACE)/ub1604-base:$(VERSION)
	docker build -f Dockerfile.ub1804 -t $(REGISTRY_NAMESPACE)/ub1804-base:$(VERSION)
	docker build -f Dockerfile.ub2004 -t $(REGISTRY_NAMESPACE)/ub2004-base:$(VERSION)
	docker build -f Dockerfile.ub2204 -t $(REGISTRY_NAMESPACE)/ub2204-base:$(VERSION)
	docker build -f Dockerfile.ub2304 -t $(REGISTRY_NAMESPACE)/ub2304-base:$(VERSION)
	docker build -f Dockerfile.ub2404 -t $(REGISTRY_NAMESPACE)/ub2404-base:$(VERSION)

clean:
	docker image rm $(REGISTRY_NAMESPACE)/ub1604-base:$(VERSION)
	docker image rm $(REGISTRY_NAMESPACE)/ub1804-base:$(VERSION)
	docker image rm $(REGISTRY_NAMESPACE)/ub2004-base:$(VERSION)
	docker image rm $(REGISTRY_NAMESPACE)/ub2204-base:$(VERSION)
	docker image rm $(REGISTRY_NAMESPACE)/ub2304-base:$(VERSION)
	docker image rm $(REGISTRY_NAMESPACE)/ub2404-base:$(VERSION)

bash:
	docker run --rm  -it $(REGISTRY_NAMESPACE)/ub1604-base bash

deploy:
	docker push $(REGISTRY_NAMESPACE)/ub1604-base:$(VERSION)
	docker push $(REGISTRY_NAMESPACE)/ub1804-base:$(VERSION)
	docker push $(REGISTRY_NAMESPACE)/ub2004-base:$(VERSION)
	docker push $(REGISTRY_NAMESPACE)/ub2204-base:$(VERSION)
	docker push $(REGISTRY_NAMESPACE)/ub2304-base:$(VERSION)
	docker push $(REGISTRY_NAMESPACE)/ub2404-base:$(VERSION)
